﻿using UnityEngine;
using System.Collections;

/* Michal */
public class TimeFlow : MonoBehaviour
{
		public static float time;
		public static string timeFormat = "0.00";

		void Start ()
		{
				time = 0.0f;
				showText ();
		}

		void Update ()
		{
				if (!Lose.isEnded) {
						time += Time.deltaTime;
						showText ();
				}
		}

		private void showText ()
		{
			guiText.text = "Time: " + time.ToString (timeFormat);
		}
}
