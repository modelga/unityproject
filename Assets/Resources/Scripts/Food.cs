﻿using UnityEngine;
using System.Collections;

//Mateusz & Michal
public class Food : MonoBehaviour
{
		public float foodValue = 1.0f;
		private bool scaleUp = false;
		private float scale = 1.0f;
		private float alpha = 1.0f;

		void Update(){
			if(scaleUp){
				scale+=0.2f;
				alpha-=0.05f; 
				this.transform.localScale = new Vector3(scale,scale,scale);
				this.renderer.material.color=new Color(1.0f,1.0f,1.0f,alpha);
			}
		}

		void OnTriggerEnter2D (Collider2D other)
		{
				this.collider2D.enabled = false;
				if(!scaleUp){		
					Hunger.eat (foodValue);
					
				}
				GlobalVariables.eaten++;
				scaleUp = true;
				this.transform.position = new Vector3(this.transform.position.x,this.transform.position.y,-5.0f);
				Invoke("remove",2.0f);
		}

	private void remove(){
		Destroy (this.gameObject);
	}
}
