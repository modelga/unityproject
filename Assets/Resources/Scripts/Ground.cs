﻿using UnityEngine;
using System.Collections;

//Mateusz
public class Ground : MonoBehaviour
{
		public float deadLine = -7.0f;

		void FixedUpdate ()
		{
				this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y - GlobalVariables.scrollSpeed, this.transform.position.z);
				if (this.transform.position.y < deadLine) {
						this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y + 15.45f, this.transform.position.z);
				}
		}
}
