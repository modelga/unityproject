﻿using UnityEngine;
using System.Collections;

//Mateusz
public class Gradient : MonoBehaviour
{
		void Start ()
		{
				this.guiTexture.color = new Color (0.185f, 0.185f, 0.115f, 0.0f); 
		}

		void Update ()
		{
				float h = Hunger.getHunger ();
				float mh = Hunger.maxHunger / 6.0f;
				if (h < mh) {
						this.guiTexture.color = new Color (0.185f, 0.185f, 0.115f, (0.4f - ((h / mh) / (1.0f / 0.4f))));
				} else {
						this.guiTexture.color = new Color (0.185f, 0.185f, 0.115f, 0.0f);
				}
				if (Lose.isEnded) {			
						this.guiTexture.color = new Color (0.185f, 0.185f, 0.115f, 0.4f);
				}
		}
}
