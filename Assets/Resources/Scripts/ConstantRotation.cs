﻿using UnityEngine;
using System.Collections;

/* Michal */
public class ConstantRotation : MonoBehaviour
{
		public float degreePerFrame = -8.0f;

		void FixedUpdate ()
		{
				transform.Rotate (0.0f, 0.0f, degreePerFrame);
		}
}
