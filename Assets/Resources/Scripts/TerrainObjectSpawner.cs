﻿using UnityEngine;
using System.Collections;

/* Michal */
public class TerrainObjectSpawner : MonoBehaviour
{

		public GameObject[] terrainObjectToSpawn;
		private GameObject instObj;
		public float terrainObjectSpawnChance = 0.5f;
		public GameObject corpse;
		public float corpseSpawnChance = 0.5f;
		public GameObject tumbleweed;
		public float tumbleweedSpawnChance = 0.5f;
		public GameObject bird;
		public float birdSpawnChance = 0.5f;
		private float actualChance;


		void Start ()
		{
				actualChance = 0.0f;	
		}

		void Update ()
		{
				if (!Lose.isEnded) {
						if (terrainObjectToSpawn.Length > 0) {
								spawnTerrainObject (getRandomPrefab (), terrainObjectSpawnChance);
						}
						spawnCorpse ();
						spawnGameObject (tumbleweed, tumbleweedSpawnChance);
						spawnGameObject (bird, birdSpawnChance);
				}
		}
	
		private GameObject getRandomPrefab ()
		{
				return terrainObjectToSpawn [(int)Random.Range (0, terrainObjectToSpawn.Length)];
		}
	
		private Vector3 getRandomPosition ()
		{
				return new Vector3 (Random.Range (-10.0f, 10.0f), 6, 0);
		}
		
		private void spawnTerrainObject (GameObject objectToSpawn, float chance)
		{
			actualChance = (TimeFlow.time/100.0f)*chance;
			if(actualChance>chance){
				actualChance = chance;
			}
			if (Random.Range (0.0f, 1.0f) < actualChance) {
				Instantiate (objectToSpawn, getRandomPosition (), Quaternion.identity);
			}
		}
		
		private void spawnCorpse ()
		{
			if (Random.Range (0.0f, 1.0f) < corpseSpawnChance) {
				Instantiate (corpse, getRandomPosition (), Quaternion.identity);
			}
		}

		private void spawnGameObject (GameObject objectToSpawn, float chance)
		{
				if (Random.Range (0.0f, 1.0f) < chance) {			
						Instantiate (objectToSpawn, Vector3.zero, Quaternion.identity);
				}
		}

}
