﻿using UnityEngine;
using System.Collections;

/* Michal */
public class SpaceToRestart : MonoBehaviour
{
		void Start ()
		{
				this.guiText.text = "";
		}

		void Update ()
		{
				if (Lose.isEnded) {
						this.guiText.text = "Click spacebar to restart game";
						if (Input.GetKeyDown (KeyCode.Space)) {
								Application.LoadLevel (Application.loadedLevel);
						}
				}
		}
}
