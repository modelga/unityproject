﻿using UnityEngine;
using System.Collections;

//Mateusz
public class Best : MonoBehaviour
{	
		public static float bestTime;
		public static int bestEat;

		void Start ()
		{
				
				if (PlayerPrefs.HasKey ("bestTime")) {
						bestTime = PlayerPrefs.GetFloat ("bestTime");
				} else {
						bestTime = 0.0f;
						PlayerPrefs.SetFloat ("bestTime", 0.0f);
				}

				if (PlayerPrefs.HasKey ("bestEat")) {
						bestEat = PlayerPrefs.GetInt ("bestEat");
				} else {
						bestEat = 0;
						PlayerPrefs.SetInt ("bestEat", 0);
				}
		}
		
		void Update ()
		{
				if (bestTime < TimeFlow.time) {
						bestTime = TimeFlow.time;
				}
				if (bestEat < GlobalVariables.eaten) {
						bestEat = GlobalVariables.eaten;	
				}
				this.guiText.text = "Best Time: " + bestTime.ToString (TimeFlow.timeFormat) + "\r\n" + 
						"Best Eaten: " + bestEat;
		}
}
