﻿using UnityEngine;
using System.Collections;

/* Michal */
public class Tumbleweed : MonoBehaviour
{
		public float speedX = 0.05f;
		public float respawnMaxRange = 5.0f;
		public float respawnMinRange = -5.0f;

		void FixedUpdate ()
		{
				this.transform.position = new Vector3 (this.transform.position.x + speedX, this.transform.position.y, 0);
				if (isDone ()) {
						Destroy (this.gameObject);
				}
		}

		void Start ()
		{		
				this.transform.position = new Vector3 (-11, Random.Range (respawnMaxRange, respawnMinRange), 0);
		}

		private bool isDone ()
		{
				if (this.transform.position.x > 11) {
						return true;
				}
				return false;
		}

}
