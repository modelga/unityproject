﻿using UnityEngine;
using System.Collections;

/* Michal */
public class ExitOnEscKey : MonoBehaviour {
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit();
		}
	}
}
