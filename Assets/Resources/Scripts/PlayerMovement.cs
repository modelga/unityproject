﻿using UnityEngine;
using System.Collections;

//Mateusz
public class PlayerMovement : MonoBehaviour
{	
		public float moveSpeed = 20.0f;
		private Component sprite;

		void Start ()
		{
	     		particleSystem.renderer.sortingLayerName = "objects";
				sprite = new Component ();
				sprite = FindChild ("sprite");
				sprite.renderer.material.color = Color.white;
		}

		void Update ()
		{ 
				if (this.transform.position.y < -7.0f) {
						Lose.endGame ();
				}
		}

		void FixedUpdate ()
		{
				if (!Lose.isEnded) {
						rigidbody2D.velocity = new Vector2 (Input.GetAxis ("Horizontal") * moveSpeed, Input.GetAxis ("Vertical") * moveSpeed);
				} else {
						rigidbody2D.velocity = new Vector2 (0, 0);
						sprite.renderer.material.color = Color.red;
				}
		}

		private Component FindChild (string name)
		{
				if (this.name == name) {
						return this;
				}
				Component[] tmp = this.GetComponentsInChildren<Component> ();
				foreach (Component t in tmp) {
						if (t.name == name) {
								return t;
						}
				}
				return null;
		}
}
