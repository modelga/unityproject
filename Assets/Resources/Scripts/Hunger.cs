﻿using UnityEngine;
using System.Collections;

/* Michal */
public class Hunger : MonoBehaviour
{
		public static float maxHunger = 100.0f;
		public float hungerSpeed = 1.0f;
		private static float hunger;
		private Component[] icons;

		void Start ()
		{
				hunger = maxHunger;
				icons = new Component[4];
				for (int i=0; i<icons.Length; i++) {
						icons [i] = new Component ();
				}
				icons [0] = FindIcon ("icon0");
				icons [1] = FindIcon ("icon1");
				icons [2] = FindIcon ("icon2");
				icons [3] = FindIcon ("icon3");				
		}

		void FixedUpdate ()
		{
				if (!Lose.isEnded) {
						if (hunger > -1) {
								hunger -= hungerSpeed;
						}
						for (int i=0; i<icons.Length; i++) {
								setApple (i);
						}
				}
				if (hunger < 0) {
						Lose.endGame ();
				}
		}

		private Component FindIcon (string name)
		{
				if (this.name == name) {
						return this;
				}
				Component[] tmp = this.GetComponentsInChildren<Component> ();
				foreach (Component t in tmp) {
						if (t.name == name) {
								return t;
						}
				}
				return null;
		}

		private void setApple (int index)
		{
				icons [index].guiTexture.color = new Color (0.5f, 0.5f, 0.5f, calculateAlpha (index));
		}

		private float getInterval ()
		{
				return (maxHunger / icons.Length);
		}

		private float calculateAlpha (int index)
		{
				float min = index * getInterval ();
				return (hunger - min) / getInterval ();
		}
	
		public static void eat (float value)
		{
				hunger += + value;
				if (hunger > maxHunger) {
						hunger = maxHunger;
				}
		}

		public static float getHunger ()
		{
				return hunger;
		}
}
