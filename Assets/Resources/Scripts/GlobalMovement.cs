﻿using UnityEngine;
using System.Collections;

//Mateusz
public class GlobalMovement : MonoBehaviour
{
		public float destroyUnder = -10.0f;

		void FixedUpdate ()
		{
				if (this.transform.position.y < destroyUnder) {
						Destroy (this.gameObject);
				}
				this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y - GlobalVariables.scrollSpeed, this.transform.position.z);
		}
}
