﻿using UnityEngine;
using System.Collections;

/* Michal */
public class Bird : MonoBehaviour
{
		public float speed = 1.0f;
		public float spawnChance = 0.001f;
		private float respawnX = 10.0f;
		private float respawnY = 7.0f;
		private Vector3 flightVector = new Vector3 (0.0f, 0.0f, 0.0f);

		void Start ()
		{
				Vector3 startPosition = getStartPosition ();
				Vector3 directionVector = getDirection ();
				flightVector = new Vector3 (directionVector.x - startPosition.x, directionVector.y - startPosition.y, 0);
				flightVector.Normalize ();
				transform.position = startPosition;
				transform.rotation = Quaternion.Euler (0, 0, -90 + (Mathf.Atan2 (flightVector.y, flightVector.x) * (180 / Mathf.PI)));
		}

		void FixedUpdate ()
		{
				if (isFlying ()) {
						transform.position = new Vector3 (transform.position.x + (flightVector.x * speed), transform.position.y + (flightVector.y * speed), 0.0f);
				} else {
						Destroy (this.gameObject);
				}
		}

		private Vector3 getStartPosition ()
		{
				int edge = Random.Range (0, 3);		
				if (edge == 0) {
						return new Vector3 (Random.Range (-respawnX, respawnX), respawnY, 0);
				} else if (edge == 1) {
						return new Vector3 (respawnX, Random.Range (-respawnY, respawnY), 0);
				} else if (edge == 2) {
						return new Vector3 (Random.Range (-respawnX, respawnX), -respawnY, 0);
				} else {
						return new Vector3 (-respawnX, Random.Range (-respawnY, respawnY), 0);
				}
		}

		private Vector3 getDirection ()
		{
				return new Vector3 (Random.Range (-(respawnX - 1.0f), (respawnX - 1.0f)), Random.Range (-(respawnY - 1.0f), (respawnY - 1.0f)), 0);
		}

		private bool isFlying ()
		{
				if (transform.position.x > respawnX || transform.position.x < -respawnX) {
						return false;
				} else if (transform.position.y > respawnY || transform.position.y < -respawnY) {
						return false;
				}
				return true;
		}
}
