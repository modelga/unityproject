﻿using UnityEngine;
using System.Collections;

//Mateusz
public class Lose : MonoBehaviour {

	public static bool isEnded;
	private static Component endMessage;

	void Start(){
		isEnded = false;
		endMessage = FindChild("endMessage");
	}

	public static void endGame () {
		isEnded = true;
		endMessage.gameObject.SetActive(true);
		GlobalVariables.scrollSpeed = 0.0f;
	}

	private Component FindChild (string name)
	{
		if (this.name == name) {
			return this;
		}
		Component[] tmp = this.GetComponentsInChildren<Component> ();
		foreach (Component t in tmp) {
			if (t.name == name) {
				return t;
			}
		}
		return null;
	}
}
