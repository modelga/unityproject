﻿using UnityEngine;
using System.Collections;

//Mateusz
public class EndMessage : MonoBehaviour
{

		public GameObject timeText;
		public GameObject countText;
		public GameObject newRecord;

		void Start ()
		{
				this.guiText.text = "Game Over!";
				this.gameObject.SetActive (false);
				newRecord.guiText.text = "NEW RECORD!";
				newRecord.SetActive (false);
		}

		void Update ()
		{
				if (Lose.isEnded) {
						timeText.guiText.text = "your time: " + TimeFlow.time.ToString (TimeFlow.timeFormat);
						countText.guiText.text = "eat count: " + GlobalVariables.eaten.ToString ();
						if (TimeFlow.time > PlayerPrefs.GetFloat ("bestTime")) {
								newRecord.SetActive (true);
								PlayerPrefs.SetFloat ("bestTime", TimeFlow.time);
						}
						if (GlobalVariables.eaten > PlayerPrefs.GetInt ("bestEat")) {
								newRecord.SetActive (true);
								PlayerPrefs.SetInt ("bestEat", GlobalVariables.eaten);
						}
				}
		}
}
